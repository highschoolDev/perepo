import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

/**
 * Created by RazB on 27/01/2017.
 */
public final class TranslationUtils {
    private TranslationUtils() {
        //Empty by intention
    }

    public static String getText(String key) {
        try {
            return new String(ResourceBundle.getBundle("ui-text").getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Failed to load " + key);
        }
    }
}
