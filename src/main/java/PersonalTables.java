import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by RazB on 27/01/2017.
 */
public class PersonalTables {

    private static Map<Integer, PersonalTable> maleTableMap = new HashMap<>(8);
    private static Map<Integer, PersonalTable> femaleTableMap = new HashMap<>(8);
    private static final String[] maleNames = {"בנים ריצת 600 מטר", "בנים הדיפת כדור ברזל 3 ק\"ג", "בנים ריצת 800 מטר", "בנים הדיפת כדור ברזל 4 ק\"ג", "בנים ריצת 60 מטר", "בנים קפיצה לרוחק", "בנים זריקת כדור יד", "בנים שליחים היקפי 75x4"};
    private static final String[] femaleNames = {"בנות ריצת 600 מטר", "בנות הדיפת כדור ברזל 3 ק\"ג", "בנות הדיפת כדור ברזל 2 ק\"ג", "בנות ריצת 60 מטר", "בנות קפיצה לרוחק", "בנות זריקת כדור יד", "בנות שליחים היקפי 75x4"};

    public PersonalTables(Scores scores) {
        for (int i = 0; i < 6; i++) {
            int boysTableNum;
            int girlsTableNum;
            boolean bNinthGraders = false;
            if (Application.grade.equals("ט'")) {
                bNinthGraders = true;
                boysTableNum = i + 2;
                if (i == 0) {
                    girlsTableNum = 0;
                } else {
                    girlsTableNum = i + 9;
                }
            } else {
                if (i >= 2) {
                    boysTableNum = i + 2;
                    girlsTableNum = i + 9;
                } else {
                    boysTableNum = i;
                    girlsTableNum = i + 8;
                }
            }
            String maleName, femaleName;
            if (bNinthGraders) {
                maleName = maleNames[i + 2];
                if (i >= 2) {
                    femaleName = femaleNames[i + 1];
                } else {
                    femaleName = femaleNames[i];
                }
            } else {
                if (i >= 1) {
                    femaleName = femaleNames[i + 1];
                } else {
                    femaleName = femaleNames[i];
                }
                if (i >= 2) {
                    maleName = maleNames[i + 2];
                } else {
                    maleName = maleNames[i];
                }
            }
            maleTableMap.put(i, new PersonalTable(scores, Gender.MALE, boysTableNum, maleName));
            femaleTableMap.put(i, new PersonalTable(scores, Gender.FEMALE, girlsTableNum, femaleName));
        }
    }

    public static JTable get(int selectedType, Gender gender) {

        switch (gender) {
            case MALE:
                return maleTableMap.get(selectedType);
            case FEMALE:
                return femaleTableMap.get(selectedType);
            default:
                return null;
        }
    }

    public PersonalTable[] getTables() {
        PersonalTable[] tables = new PersonalTable[femaleTableMap.size() + maleTableMap.size() - 1];
        for (Map.Entry<Integer, PersonalTable> entry : femaleTableMap.entrySet()) {
            int key = entry.getKey();
            tables[key] = entry.getValue();
        }
        for (Map.Entry<Integer, PersonalTable> entry : maleTableMap.entrySet()) {
            int key = entry.getKey();
            tables[key + femaleTableMap.size() - 1] = entry.getValue();
        }
        return tables;
    }
}


