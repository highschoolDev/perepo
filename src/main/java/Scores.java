/**
 * Created by RazB on 18/01/2017.
 */
public class Scores {
    private int[][] scores = new int[14][5];
    private Object[] row = new Object[5];

    public int[] getScores(int index) {
        return scores[index];
    }

    public void setScores(int row, int[] newScores) {
        this.scores[row] = newScores;
    }

    public int getScore(int row, int col) {
        return scores[row][col];
    }

    public void zeroArray() {
        for (int i = 0; i < scores.length; i++) {
            for (int j = 0; j < scores[i].length; j++) {
                this.scores[i][j] = 0;
            }
        }
    }

    public void addScore(int row, int col, int score) {
        this.scores[row][col] += score;
    }

    public void subtractScore(int row, int col, int score) {
        this.scores[row][col] -= score;
    }

    public Object[] getRow() {
        return row;
    }

    public void setRow(Object[] row) {
        this.row = row;
    }

    public void update(int row, int column, int score) {
        scores[row][column] = score;
    }
}
