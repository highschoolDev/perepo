/*
 * Copyright (c) 2017. name
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

/**
 * Created by RazB on 02/03/2017.
 */
public class ResultArrays {
    private static double[][] arr;

    static {
        try {
            arr = ReadWriteExcelFile.getDataXLSX();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double smartRounding(double source, int digits) {
        return Math.round(source * Math.pow(10, digits)) / Math.pow(10, digits);
    }

    public static int getClosestResult(String result, int type, int reference) {
        Double key = Double.parseDouble(result);
        double[] currentArray = arr[reference];
        int length = (int) currentArray[0];
        int[] baseScore = new int[length];
        for (int i = 0; i < baseScore.length; i++) {
            baseScore[i] = (int) currentArray[i + 1];
        }
        double[] newArray = new double[currentArray.length - baseScore.length - 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = currentArray[baseScore.length + i + 1];
        }
        int low = 0;
        int high = newArray.length - 1;//2

        while (low < high) {
            int mid = (low + high) / 2;
            assert (mid < high);
            double d1 = smartRounding(Math.abs(newArray[mid] - key), 3);
            double d2 = smartRounding(Math.abs(newArray[mid + 1] - key), 3);
            if (d2 <= d1) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        if (length == 1) {
            return high + baseScore[0];
        } else {
            return baseScore[high];
        }
    }
}
