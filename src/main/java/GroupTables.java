import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by RazB on 27/01/2017.
 */
public class GroupTables {

    private static Map<Integer, GroupTable> tableMap = new HashMap<>(4);
    private static final String[] names = {"שליחים נגדי", "קפיצה מהמקום", "קפיצה בדלגית", "נשיאת כדור כוח", "ריצה קבוצתית"};

    public GroupTables(Scores scores) {
        for (int i = 0; i < names.length; i++) {
            tableMap.put(i, new GroupTable(scores, 15));
            tableMap.get(i).setName(names[i]);
        }
    }

    public static JTable get(int selectedType) {
        return tableMap.get(selectedType);
    }


    public GroupTable[] getTables() {
        GroupTable[] tables = new GroupTable[tableMap.size()];
        for (Map.Entry<Integer, GroupTable> entry : tableMap.entrySet()) {
            int key = entry.getKey();
            tables[key] = entry.getValue();
        }
        return tables;
    }
}
