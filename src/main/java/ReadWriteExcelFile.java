import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadWriteExcelFile {

    /*private static String[] dataPaths={
            "DataSheets/boys600m.csv",
            "DataSheets/boysIronBall3kg.csv",
            "DataSheets/boys800m.csv",
            "DataSheets/boysIronBall4kg.csv",
            "DataSheets/boys60m.csv",
            "DataSheets/boysJumpAfar.csv",
            "DataSheets/boysHandBallThrow.csv",
            "DataSheets/boysRelayRace.csv",
            "DataSheets/girls600m.csv",
            "DataSheets/girlsIronBall2kg.csv",
            "DataSheets/girlsIronBall9thGrade3kg.csv",
            "DataSheets/girls60m.csv",
            "DataSheets/girlsJumpAfar.csv",
            "DataSheets/girlsHandBallThrow.csv",
            "DataSheets/girls'RelayRace.csv",
            "DataSheets/groupActivities.csv"
    };*/

    private static String dataPathsSetting = "Settings/DataSheetsPaths.csv";

    private static String[] dataPaths = readDataPathsSetting();

    private static void makeFile(String path) {
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static <T> void makeNewDataSheet(String filename, int score, int length, T[] data) {
        String path = "DataSheets/" + filename + ".csv";
        makeFile(path);
        try {
            FileWriter writer = new FileWriter(path);
            writer.write(filename);
            writer.append(',');
            writer.write(length);
            writer.append(',');
            writer.write(score);
            for (T t : data) {
                writer.append(',');
                writer.write(t.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String[] readDataPathsSetting() {
        InputStream fileToRead = Application.class.getResourceAsStream(dataPathsSetting);
        ArrayList<String> stringArrayList = new ArrayList<>();
        Scanner scanner = new Scanner(fileToRead);
        scanner.useDelimiter(",");
        while (scanner.hasNext()) {
            stringArrayList.add(scanner.next());
        }
        scanner.close();

        if (dataPaths == null) {
            dataPaths = new String[]{};
        }
        return stringArrayList.toArray(dataPaths);
    }

    private static void writeToDataPathsSetting(String[] newDataPaths) {
        try {
            FileWriter writer = new FileWriter(dataPathsSetting, false);
            for (int i = 0; i < newDataPaths.length - 1; i++) {
                writer.write(newDataPaths[i] + ',');
            }
            writer.write(newDataPaths[newDataPaths.length - 1]);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void readXLSFile(String xlsPath, JTable table) throws IOException {
        InputStream ExcelFileToRead = new FileInputStream(xlsPath);
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row;
        HSSFCell cell;

        for (int i = 1; i <= table.getRowCount(); i++) {
            row = sheet.getRow(i);
            for (int j = 0; j < table.getColumnCount(); j++) {
                cell = row.getCell(j);
                if (!cell.getStringCellValue().equals("null")) {
                    table.setValueAt(cell.getStringCellValue() + "", i - 1, j);
                }
            }
        }

    }

    static void writeXLSFile(String xlsPath, String name, JTable table) throws IOException {

        String excelFileName = xlsPath + ".xls";//name of excel file

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet(name);

        TableModel model = table.getModel(); //Table model

        HSSFRow headerRow = sheet.createRow(0); //Create header row
        for (int headings = 0; headings < model.getColumnCount(); headings++) { //For each column
            headerRow.createCell(headings).setCellValue(model.getColumnName(headings));//Write column name
        }

        for (int r = 1; r <= model.getRowCount(); r++) { //For each table row
            HSSFRow row = sheet.createRow(r);
            for (int col = 0; col < model.getColumnCount(); col++) { //For each table column
                HSSFCell cell = row.createCell(col); // create a cell in the row for each column
                cell.setCellValue("" + model.getValueAt(r - 1, col));//Write value
            }
        }

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an OutputStream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    static void writeXLSFileWithSheets(String xlsPath, JTable[][] tables) throws IOException {

        String excelFileName = xlsPath + ".xls";//name of excel file

        //String sheetName = name;//name of sheet

        HSSFWorkbook wb = new HSSFWorkbook();
        for (JTable[] jTables : tables) {
            for (JTable table : jTables) {
                String sheetName = table.getName();
                HSSFSheet sheet = wb.createSheet(sheetName);

                TableModel model = table.getModel(); //Table model

                HSSFRow headerRow = sheet.createRow(0); //Create header row
                for (int headings = 0; headings < model.getColumnCount(); headings++) { //For each column
                    headerRow.createCell(headings).setCellValue(model.getColumnName(headings));//Write column name
                }

                for (int r = 1; r <= model.getRowCount(); r++) { //For each table row
                    HSSFRow row = sheet.createRow(r);
                    for (int col = 0; col < model.getColumnCount(); col++) { //For each table column
                        HSSFCell cell = row.createCell(col); // create a cell in the row for each column
                        cell.setCellValue("" + model.getValueAt(r - 1, col));//Write value
                    }
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an OutputStream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    static void readXLSXFile(String xlsxPath, JTable table) throws IOException {
        InputStream excelFileToRead = new FileInputStream(xlsxPath);
        XSSFWorkbook wb = new XSSFWorkbook(excelFileToRead);

        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFRow row;
        XSSFCell cell;

        for (int i = 1; i <= table.getRowCount(); i++) {
            row = sheet.getRow(i);
            for (int j = 0; j < table.getColumnCount() - 1; j++) {
                cell = row.getCell(j);
                if (!cell.getStringCellValue().equals("null")) {
                    table.setValueAt(cell.getStringCellValue() + "", i - 1, j);
                } else if (j == table.getColumnCount() - 2) {
                    table.setValueAt("0", i - 1, j);
                } else {
                    table.setValueAt("", i - 1, j);
                }
            }
        }
    }

    static void writeXLSXFile(String xlsxPath, JTable table) throws IOException {

        String excelFileName = xlsxPath + ".xlsx";

        XSSFWorkbook wb = new XSSFWorkbook();

        createXLSXSheet(table, wb);

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an Outputstream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    static void writeXLSXFileWithSheets(String xlsPath, JTable[][] tables) throws IOException {

        String excelFileName = xlsPath + ".xlsx";//name of excel file

        XSSFWorkbook wb = new XSSFWorkbook();
        for (JTable[] jTables : tables) {
            for (JTable table : jTables) {
                createXLSXSheet(table, wb);
            }
        }

        FileOutputStream fileOut = new FileOutputStream(excelFileName);

        //write this workbook to an OutputStream.
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    static void createXLSXSheet(JTable table, XSSFWorkbook wb) {
        String sheetName = table.getName();
        XSSFSheet sheet = wb.createSheet(sheetName);

        TableModel model = table.getModel(); //Table model

        XSSFRow headerRow = sheet.createRow(0); //Create header row
        for (int headings = 0; headings < model.getColumnCount(); headings++) { //For each column
            headerRow.createCell(headings).setCellValue(model.getColumnName(headings));//Write column name
        }

        for (int r = 1; r <= model.getRowCount(); r++) { //For each table row
            XSSFRow row = sheet.createRow(r);
            for (int col = 0; col < model.getColumnCount(); col++) { //For each table column
                XSSFCell cell = row.createCell(col); // create a cell in the row for each column
                cell.setCellValue("" + model.getValueAt(r - 1, col));//Write value
            }
        }
    }

    static double[][] getDataXLSX() throws IOException {
//        String[] dataPaths={
//                "DataSheets/boys600m.csv",
//                "DataSheets/boysIronBall3kg.csv",
//                "DataSheets/boys800m.csv",
//                "DataSheets/boysIronBall4kg.csv",
//                "DataSheets/boys60m.csv",
//                "DataSheets/boysJumpAfar.csv",
//                "DataSheets/boysHandBallThrow.csv",
//                "DataSheets/boysRelayRace.csv",
//                "DataSheets/boys600m.csv",
//                "DataSheets/girlsIronBall2kg.csv",
//                "DataSheets/girlsIronBall9thGrade3kg.csv",
//                "DataSheets/girls60m.csv",
//                "DataSheets/girlsJumpAfar.csv",
//                "DataSheets/girlsHandBallThrow.csv",
//                "DataSheets/girls'RelayRace.csv",
//                "DataSheets/groupActivities.csv"
//        };//needs to be in resources
        ArrayList<double[]> dataArray = new ArrayList<>(12);//initial capacity needs to be loaded from resources
        for (int i = 0; i < dataPaths.length; i++) {
            InputStream fileToRead = Application.class.getResourceAsStream(dataPaths[i]);
            Workbook wb;
            Sheet sheet;
            if (dataPaths[i].endsWith(".xlsx")) {
                wb = new XSSFWorkbook(fileToRead);
            } else if (dataPaths[i].endsWith(".xls")) {
                wb = new HSSFWorkbook(fileToRead);
            } else if (dataPaths[i].endsWith(".csv")) {
                Scanner scanner = new Scanner(fileToRead);
                scanner.useDelimiter(",");
                scanner.next();
                double[] arr = new double[Integer.parseInt(scanner.next())];
                for (int j = 0; scanner.hasNext(); j++) {
                    arr[j] = Double.parseDouble(scanner.next());
                }
                scanner.close();
                dataArray.add(arr);
                continue;
            } else {
                continue;
            }
            sheet = wb.getSheetAt(0);
            Row row;
            Cell cell;

            double[] arr = new double[sheet.getLastRowNum()];
            for (int j = 1; j < sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                cell = row.getCell(0);
                arr[j] = cell.getNumericCellValue();
            }
            dataArray.add(i, arr);
        }
        double[][] dataArray2D = new double[dataArray.size()][];
        for (int i = 0; i < dataArray2D.length; i++) {
            double[] row = dataArray.get(i);
            dataArray2D[i] = row;
        }
        return dataArray2D;
    }

    public static String getNameOfTable(int tableNum) {
        InputStream excelFileToRead = Application.class.getResourceAsStream(dataPaths[tableNum]);
        String name = "";
        if (dataPaths[tableNum].endsWith(".csv")) {
            Scanner scanner = new Scanner(excelFileToRead);
            scanner.useDelimiter(",");
            name = scanner.next();
        }
        return name;
    }

}