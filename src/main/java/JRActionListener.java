import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by RazB on 27/01/2017.
 */
public class JRActionListener implements ActionListener {
    private MyJMenuBar myJMenuBar;

    public JRActionListener(MyJMenuBar myJMenuBar) {
        this.myJMenuBar = myJMenuBar;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "print":
                Application.printJTable();
                break;
            case "sorted print":
                Application.printSortedJTable();
                break;
            case "export":
                Application.exportJTable();
                break;
            case "export all":
                Application.exportAllJTables();
                break;
            case "open":
                Application.loadJTable();
                break;
            case "settings":
//            Application.showSettings();
                break;
            case "exit":
                System.exit(0);
            default:  //table code
                myJMenuBar.setSelectedType(Integer.parseInt(e.getActionCommand().substring(0, 1)));
                myJMenuBar.setSelectedMenu(Integer.parseInt(e.getActionCommand().substring(1, 2)));
                break;
        }
    }
}
