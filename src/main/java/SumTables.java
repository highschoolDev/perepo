import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by RazB on 27/01/2017.
 */
public class SumTables {

    private static Map<Integer, SumTable> tableMap = new HashMap<>(3);
    private final static String[] names = {"סיכום אישיים בנים", "סיכום אישיים בנות", "סיכום אישיים בנים ובנות", "סיכום קבוצתיים", "סידור לפי ניקוד סופי"};

    public SumTables() {
        for (int i = 0; i < names.length; i++) {
            tableMap.put(i, new SumTable(i));
            tableMap.get(i).setName(names[i]);
        }
    }

    public static JTable get(int selectedType) {
        return tableMap.get(selectedType);
    }

    public void updateAll(Scores scores) {
        for (int i = 0; i < tableMap.size(); i++) {
            tableMap.get(i).update(scores, i);
        }
    }

    public SumTable[] getTables() {
        SumTable[] tables = new SumTable[tableMap.size()];
        for (Map.Entry<Integer, SumTable> entry : tableMap.entrySet()) {
            int key = entry.getKey();
            tables[key] = entry.getValue();
        }
        return tables;
    }
}
