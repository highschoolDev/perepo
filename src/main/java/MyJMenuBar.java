import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Created by RazB on 22/01/2017.
 */
class MyJMenuBar {

    private JMenuBar menuBar;
    private int selectedType;
    private int selectedMenu;

    MyJMenuBar() {
        menuBar = new JMenuBar();
        JRActionListener myListener = new JRActionListener(this);
        JMenu fileMenu = new JMenu(TranslationUtils.getText("file.menu.name"));
        fileMenu.getAccessibleContext().setAccessibleDescription("הדפסה ועוד דברים");

        JMenuItem printing = new JMenuItem("הדפס");
        printing.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
        printing.setActionCommand("print");
        printing.addActionListener(myListener);
        fileMenu.add(printing);

        JMenuItem sortedPrinting = new JMenuItem("הדפס לפי דירוג");
        sortedPrinting.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.SHIFT_MASK | InputEvent.CTRL_MASK));
        sortedPrinting.setActionCommand("sorted print");
        sortedPrinting.addActionListener(myListener);
        fileMenu.add(sortedPrinting);

        JMenuItem exporting = new JMenuItem("שמור/יצא");
        exporting.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        exporting.setActionCommand("export");
        exporting.addActionListener(myListener);
        fileMenu.add(exporting);

        JMenuItem exportAll = new JMenuItem("שמור/יצא הכל");
        exportAll.setActionCommand("export all");
        exportAll.addActionListener(myListener);
        fileMenu.add(exportAll);

        JMenuItem open = new JMenuItem("פתח/יבא");
        open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        open.setActionCommand("open");
        open.addActionListener(myListener);
        fileMenu.add(open);

        JMenuItem settings = new JMenuItem("הגדרות");
        settings.setActionCommand("settings");
        settings.addActionListener(myListener);
        fileMenu.add(settings);

        JMenuItem exit = new JMenuItem("יציאה");
        exit.setActionCommand("exit");
        exit.addActionListener(myListener);
        fileMenu.add(exit);

        menuBar.add(fileMenu);
        JMenu personalTables = new JMenu(TranslationUtils.getText("personal.menu.name"));
        personalTables.getAccessibleContext().setAccessibleDescription("הטבלאות של המקצים האישיים");
        menuBar.add(personalTables);

        //boysGroup
        JMenu boys = new JMenu("בנים");

        ButtonGroup group = new ButtonGroup();

        JRadioButtonMenuItem rbMenuItem1;
        if (Application.grade.equals("ט'")) {
            rbMenuItem1 = new JRadioButtonMenuItem("ריצת 800 מטר");
        } else {
            rbMenuItem1 = new JRadioButtonMenuItem("ריצת 600 מטר");
        }
        rbMenuItem1.setActionCommand("00");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        rbMenuItem1.setSelected(true);
        boys.add(rbMenuItem1);
        personalTables.add(boys);

        if (Application.grade.equals("ט'")) {
            rbMenuItem1 = new JRadioButtonMenuItem("הדיפת כדור ברזל 4 ק\"ג");
        } else {
            rbMenuItem1 = new JRadioButtonMenuItem("הדיפת כדור ברזל 3 ק\"ג");
        }
        rbMenuItem1.setActionCommand("10");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        boys.add(rbMenuItem1);


        rbMenuItem1 = new JRadioButtonMenuItem("ריצת 60 מטר");
        rbMenuItem1.setActionCommand("20");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        boys.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("קפיצה לרוחק");
        rbMenuItem1.setActionCommand("30");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        boys.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("זריקת כדור יד");
        rbMenuItem1.setActionCommand("40");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        boys.add(rbMenuItem1);


        rbMenuItem1 = new JRadioButtonMenuItem("שליחים היקפי 75*4");
        rbMenuItem1.setActionCommand("50");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        boys.add(rbMenuItem1);
        personalTables.add(boys);

        personalTables.addSeparator();

        JMenu girls = new JMenu("בנות");

        rbMenuItem1 = new JRadioButtonMenuItem("ריצת 600 מטר");
        rbMenuItem1.setActionCommand("01");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);
        personalTables.add(girls);

        if (Application.grade.equals("ט'")) {
            rbMenuItem1 = new JRadioButtonMenuItem("הדיפת כדור ברזל 3 ק\"ג");
        } else {
            rbMenuItem1 = new JRadioButtonMenuItem("הדיפת כדור ברזל 2 ק\"ג");
        }
        rbMenuItem1.setActionCommand("11");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("ריצת 60 מטר");
        rbMenuItem1.setActionCommand("21");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("קפיצה לרוחק");
        rbMenuItem1.setActionCommand("31");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("זריקת כדור יד");
        rbMenuItem1.setActionCommand("41");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("שליחים היקפי 75*4");
        rbMenuItem1.setActionCommand("51");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        girls.add(rbMenuItem1);
        personalTables.add(girls);

        JMenu groupActivities = new JMenu("קבוצתיים");
        groupActivities.getAccessibleContext().setAccessibleDescription("הטבלאות של המקצים הקבוצתיים");
        menuBar.add(groupActivities);

        rbMenuItem1 = new JRadioButtonMenuItem("שליחים נגדי");
        rbMenuItem1.setActionCommand("02");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        groupActivities.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("קפיצה מהמקום");
        rbMenuItem1.setActionCommand("12");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        groupActivities.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("קפיצה בדלגית");
        rbMenuItem1.setActionCommand("22");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        groupActivities.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("נשיאת כדור כוח");
        rbMenuItem1.setActionCommand("32");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        groupActivities.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("ריצה קבוצתית");
        rbMenuItem1.setActionCommand("42");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        groupActivities.add(rbMenuItem1);

        JMenu sumTables = new JMenu("סיכומים");
        sumTables.getAccessibleContext().setAccessibleDescription("הטבלאות של הסיכומים");
        menuBar.add(sumTables);

        rbMenuItem1 = new JRadioButtonMenuItem("סיכום אישיים בנים");
        rbMenuItem1.setActionCommand("03");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        sumTables.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("סיכום אישיים בנות");
        rbMenuItem1.setActionCommand("13");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        sumTables.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("סיכום אישיים בנים/בנות");
        rbMenuItem1.setActionCommand("23");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        sumTables.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("סיכום קבוצתיים");
        rbMenuItem1.setActionCommand("33");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        sumTables.add(rbMenuItem1);

        rbMenuItem1 = new JRadioButtonMenuItem("סידור לפי ניקוד סופי");
        rbMenuItem1.setActionCommand("43");
        rbMenuItem1.addActionListener(myListener);
        group.add(rbMenuItem1);
        sumTables.add(rbMenuItem1);
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public int getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(int selectedType) {
        this.selectedType = selectedType;
    }

    public int getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(int selectedMenu) {
        this.selectedMenu = selectedMenu;
    }
}
