import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by RazB on 27/01/2017.
 */
public class SumTable extends JTable {
    private static final String[] columnNames = {"כיתה", "נקודות"};
    private Object[][] data;
    //private String[] race;

    public SumTable(int type) {

        //really needs improvement
        data = new Object[14][2];
        int classNum = 1;
        for (int i = 1; i < 15; i++) {
            data[i - 1][0] = String.format("כיתה %s %d", Application.grade, classNum);
            classNum++;
            data[i - 1][1] = 0;
        }
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        setModel(model);
        setMaximumSize(new Dimension(800, 600));
        setColumnSelectionAllowed(false);
        setRowSelectionAllowed(false);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    } // whole table should'nt be editable

    public void update(Scores scores, int type) {
        boolean bRearrange = false;
        for (int i = 0; i < 14; i++) {
            int newVal = scores.getScore(i, type);
            if ((int) data[i][1] != newVal) {
                bRearrange = true;
                data[i][1] = newVal;
                setValueAt(newVal, i, 1);
            }
        }
        if (bRearrange) {
            rearrange(scores);
        }
    }

    public void rearrange(Scores scores) {
        Object[][] sortedData = data.clone();
        Arrays.sort(sortedData, Comparator.comparing((Object[] entry) -> (int) entry[1]).reversed());
        for (int i = 0; i < 14; i++) {
            for (int j = 0; j < 2; j++) {
                setValueAt(sortedData[i][j], i, j);
                //System.out.println(String.format("sortedData[i=%d][j=%d] = %s",i,j,sortedData[i][j]));
                //System.out.println(String.format("data[i=%d][j=%d] = %s",i,j,data[i][j]));
            }
            //System.out.println();
        }
    }

}
