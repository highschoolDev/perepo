import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by RazB on 18/01/2017.
 */
public class PersonalTable extends JTable {
    private static final String[] columnNames = {"שם",
            "שם משפחה",
            "כיתה",
            "תוצאה",
            "ניקוד"
    };
    private Object[][] data;
    private int tableNum;
    //private String race;
    //private boolean change;
    private TableModelListener listener;
    private Gender gender;
    private Scores scores;
    private int CONTESTANTS_FOR_EACH_CLASS = 2;

    public PersonalTable(Scores scores, Gender gender, int tableNum, String name) {

        //really needs improvement
        this.tableNum = tableNum;
        this.gender = gender;
        this.scores = scores;
        setName(name);
        if (getName().contains("שליחים היקפי 75x4")) {
            CONTESTANTS_FOR_EACH_CLASS = 1;
        }
        data = new Object[14 * CONTESTANTS_FOR_EACH_CLASS][5];
        int classNum = 1;
        for (int i = 1; i < data.length + 1; i++) {
            data[i - 1][2] = String.format("כיתה %s %d", Application.grade, classNum);
            if (i % CONTESTANTS_FOR_EACH_CLASS == 0 && i < data.length) {
                classNum++;
            } else if (i == data.length + 1) {
                classNum = 1;
            }
            data[i - 1][4] = "0";
            data[i - 1][3] = "0";
        }
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        setModel(model);
        setMaximumSize(new Dimension(800, 600));
        setColumnSelectionAllowed(false);
        setRowSelectionAllowed(false);

        listener = e -> {
            if (e.getType() == TableModelEvent.UPDATE || e.getType() == TableModelEvent.INSERT) {
                int selectedColumn = e.getColumn();
                int firstRow = e.getFirstRow();
                if (selectedColumn == 0 || selectedColumn == 1) {
                    data[firstRow][selectedColumn] = getValueAt(firstRow, selectedColumn);
                    return;
                }
                if (selectedColumn == 3) {
                    String newPoints;
                    if (Double.parseDouble((String) getValueAt(firstRow, selectedColumn)) == 0) {
                        newPoints = "" + 0;
                    } else {
                        newPoints = "" + ResultArrays.getClosestResult((String) getValueAt(firstRow, selectedColumn), 2, tableNum);
                    }
                    if (gender == Gender.MALE) {
                        scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 0, -Integer.parseInt((String) data[firstRow][4]));
                        scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 0, Integer.parseInt(newPoints));
                    } else {
                        scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 1, -Integer.parseInt((String) data[firstRow][4]));
                        scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 1, Integer.parseInt(newPoints));
                    }
                    scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 2, -Integer.parseInt((String) data[firstRow][4]));
                    scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 2, Integer.parseInt(newPoints));
                    scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 4, -Integer.parseInt((String) data[firstRow][4]));
                    scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 4, Integer.parseInt(newPoints));
                    data[firstRow][4] = newPoints;
                    setValueAt(newPoints, firstRow, 4);
                }
                if (data[firstRow][selectedColumn] != null) { //TODO prob wrong. need to check if it should be getValue(firstRow,selectedColumn) != null
                    if (!data[firstRow][selectedColumn].equals(getValueAt(firstRow, selectedColumn))) {
                        if (selectedColumn == 4) {
                            scores.subtractScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 2, Integer.parseInt((String) data[firstRow][selectedColumn]));
                            scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 2, Integer.parseInt((String) getValueAt(firstRow, selectedColumn)));
                            scores.subtractScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 4, Integer.parseInt((String) data[firstRow][selectedColumn]));
                            scores.addScore(firstRow / CONTESTANTS_FOR_EACH_CLASS, 4, Integer.parseInt((String) getValueAt(firstRow, selectedColumn)));
                        }
                        data[firstRow][selectedColumn] = getValueAt(firstRow, selectedColumn);
                    }
                }
            }
            Application.updateScores();
        };

        addListener();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return !(column == 4);
    } // result column should'nt be editable

    public void addListener() {
        getModel().addTableModelListener(listener);
    }

    public void removeListener() {
        getModel().removeTableModelListener(listener);
    }

    public void sortAndPrint() {
        removeListener();
        Object[][] sortedData = data.clone();
        Arrays.sort(sortedData, Comparator.comparing((Object[] entry) -> Integer.parseInt((String) entry[entry.length - 1])).reversed());
        for (int i = 0; i < sortedData.length; i++) {
            for (int j = 0; j < sortedData[i].length; j++) {
                setValueAt(sortedData[i][j], i, j);
            }
        }
        try {
            print();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                setValueAt(data[i][j], i, j);
            }
        }
        addListener();
    }
}
