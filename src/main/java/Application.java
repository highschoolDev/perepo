/*
 * Copyright (c) 2017. name
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.util.Locale;

public class Application extends JPanel {
    static String grade;
    private static JFrame frame;
    private static JTable currentTable;
    private static JTable lastTable;
    private static MyJMenuBar myJMenuBar;
    private static PersonalTables personalTables;
    private static GroupTables groupTables;
    private static JScrollPane scrollPane;
    private static SumTables sumTables;
    private static Scores scores;

    //TODO Externalize all strings (multilingual support)
    //TODO periodic file save.
    //TODO All sizes (screen size for example) should be loaded from config file or at least a constant - ehm ehm Not -> self adjusting with font 20
    ActionListener doUpdates = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            //updateScores();
            setCurrentTable();
            setScrollPane();
        }
    };

    public Application() {
        super(new GridLayout(1, 0));
        myJMenuBar = new MyJMenuBar();
        //exporter=new ExcelExporter();

        scores = new Scores();
        personalTables = new PersonalTables(scores);
        groupTables = new GroupTables(scores);
        sumTables = new SumTables();
        currentTable = personalTables.get(0, Gender.MALE);
        lastTable = currentTable;

        Timer timer = new Timer(1000 / 60, doUpdates);
        timer.start();

        //Create the scroll pane and add the table to it.
        scrollPane = new JScrollPane(lastTable);
        //scrollPane.add(lastTable);
        scrollPane.setSize(800, 600);
        lastTable.setPreferredScrollableViewportSize(new Dimension(800, lastTable.getRowHeight() * lastTable.getRowCount()));
        lastTable.setFillsViewportHeight(true);


        //Add the scroll pane to this panel.
        this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), currentTable.getName(), TitledBorder.CENTER, TitledBorder.TOP));
        add(scrollPane);
        setLayout(new FlowLayout());
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        frame = new JFrame("PEApplication");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon icon = new ImageIcon(Application.class.getResource("/icons/PEP_icon.jpg"));
        frame.setIconImage(icon.getImage());


        String[] optionFormats = new String[3];
        optionFormats[0] = "ז'";
        optionFormats[1] = "ח'";
        optionFormats[2] = "ט'";
        int choice = JOptionPane.showOptionDialog(getFrame(), "Choose grade", "grade", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionFormats, "ז'");
        if (choice != -1) {
            grade = optionFormats[choice];
        } else {
            System.exit(0);
        }


        //Create and set up the content pane.
        Application newContentPane = new Application();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);


        frame.pack();
        frame.setSize(1080, 780);
        frame.setJMenuBar(myJMenuBar.getMenuBar());
        frame.setVisible(true);
    }

    public static void updateScores() {
        sumTables.updateAll(scores);
    }

    static void printJTable() {
        try {
            currentTable.print();
        } catch (PrinterException e) {
            e.printStackTrace();
        }
    }

    static void printSortedJTable() {
        if (currentTable.getClass() == PersonalTable.class) {
            ((PersonalTable) currentTable).sortAndPrint();
        } else if (currentTable.getClass() == GroupTable.class) {
            ((GroupTable) currentTable).sortAndPrint();
        }
    }

    static void exportJTable() {
        exportJTable(currentTable);
    }

    static void exportJTable(JTable table) {

        //String name = JOptionPane.showInputDialog(getFrame(),"Enter file name", "results");
        String[] optionFormats = new String[2];
        optionFormats[0] = "xls";
        optionFormats[1] = "xlsx";
        int type = JOptionPane.showOptionDialog(getFrame(), "Choose format", "format", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionFormats, "xls");
        if (type != JOptionPane.CANCEL_OPTION && type != JOptionPane.CLOSED_OPTION) {

            /*JFileChooser fileChooser=new JFileChooser();
            fileChooser.setMinimumSize(new Dimension(800,600));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*."+optionFormats[type], optionFormats[type]);
            fileChooser.setFileFilter(filter);
            int returnVal =fileChooser.showSaveDialog(null);
            String path="";
            String name="";
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                path = fileChooser.getSelectedFile().getAbsolutePath();
                name = fileChooser.getName(fileChooser.getSelectedFile());
            }*/

            String[] params = chooseFile(optionFormats[type], "save");
            String path = params[0], name = params[1];

            //String path=String.format("%s\\Desktop\\%s",System.getProperty("user.home"),name);
            if (!name.equals("")) {
                try {
                    if (type == 0) {
                        ReadWriteExcelFile.writeXLSFile(path, name, table);
                    } else if (type == 1) {
                        ReadWriteExcelFile.writeXLSXFile(path, table);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    static void loadJTable() {
        String[] optionFormats = new String[2];
        optionFormats[0] = "xls";
        optionFormats[1] = "xlsx";
        int type = JOptionPane.showOptionDialog(getFrame(), "Choose format", "format", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionFormats, "xls");

        /*JFileChooser fileChooser=new JFileChooser();
        fileChooser.setMinimumSize(new Dimension(800,600));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*."+optionFormats[type], optionFormats[type]);
        fileChooser.setFileFilter(filter);
        int returnVal =fileChooser.showOpenDialog(null);
        String path="";
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fileChooser.getSelectedFile().getAbsolutePath();
        }
        //String path2 = JOptionPane.showInputDialog(getFrame(),"Enter file path");*/

        String path = chooseFileOneParam(optionFormats[type], "path & open");


        if (!path.equals("")) {
            try {
                if (type == 0) {
                    ReadWriteExcelFile.readXLSFile(path, currentTable);
                } else if (type == 1) {
                    ReadWriteExcelFile.readXLSXFile(path, currentTable);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //TODO implement sheets exporting & test this
    static void exportAllJTables() {

        String[] optionFormats = new String[2];
        optionFormats[0] = "xls";
        optionFormats[1] = "xlsx";
        int type = JOptionPane.showOptionDialog(getFrame(), "Choose format", "format", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, optionFormats, "xls");
        String path = chooseFileOneParam(optionFormats[type], "path & save");

        if (!path.equals("")) {
            try {
                if (type == 0) {
                    ReadWriteExcelFile.writeXLSFileWithSheets(path, new JTable[][]{personalTables.getTables(), groupTables.getTables(), sumTables.getTables()});
                } else if (type == 1) {
                    ReadWriteExcelFile.writeXLSXFileWithSheets(path, new JTable[][]{personalTables.getTables(), groupTables.getTables(), sumTables.getTables()});
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static String chooseFileOneParam(String format, String choice) {
        int option, choiceOfParam;
        if (choice.contains("path")) {
            choiceOfParam = 0;    //only path
        } else {     //(boolean)(choice.contains("name")) -> true
            choiceOfParam = 1;    //only name
        }
        return chooseFile(format, choice)[choiceOfParam];
    }

    static String[] chooseFile(String format, String choice) {
        int option;
        if (choice.contains("save")) {
            option = 0;     //save dialogue
        } else {     //(boolean)(choice.contains("open")) -> true
            option = 1;   //open dialogue
        }
        return chooseFile(format, option);
    }

    static String[] chooseFile(String format, int option) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMinimumSize(new Dimension(800, 600));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*." + format, format);
        fileChooser.setFileFilter(filter);
        int returnVal;
        if (option == 0) {
            returnVal = fileChooser.showSaveDialog(null);
        } else {
            returnVal = fileChooser.showOpenDialog(null);
        }

        String path = "";
        String name = "";
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fileChooser.getSelectedFile().getAbsolutePath();
            name = fileChooser.getName(fileChooser.getSelectedFile());
        }
        return new String[]{path, name};
    }

    public static void main(String[] args) {
        Locale.setDefault(new Locale("he"));
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public static JFrame getFrame() {
        return frame;
    }

    public void setCurrentTable() {
        int selectedMenu = myJMenuBar.getSelectedMenu();
        int selectedType = myJMenuBar.getSelectedType();
//        System.out.println("selectedMenu"+selectedMenu);
//        System.out.println("selectedType"+selectedType);
        switch (selectedMenu) {
            case 0:
                currentTable = personalTables.get(selectedType, Gender.MALE);
                break;
            case 1:
                currentTable = personalTables.get(selectedType, Gender.FEMALE);
                break;
            case 2:
                currentTable = groupTables.get(selectedType);
                break;
            case 3:
                currentTable = sumTables.get(selectedType);
                break;
            default:
                currentTable = personalTables.get(0, Gender.MALE);
                break;
        }
    }

    void setScrollPane() {
        if (!lastTable.equals(currentTable)) {
//            scrollPane.removeAll();
//            scrollPane.add(currentTable);
            scrollPane.remove(lastTable);
            currentTable.setPreferredScrollableViewportSize(new Dimension(800, (int) (currentTable.getRowHeight() * (currentTable.getRowCount() + 1.5))));
            currentTable.setFillsViewportHeight(false);
            lastTable = currentTable;
            scrollPane.setViewportView(lastTable);
            scrollPane.setSize(currentTable.getPreferredScrollableViewportSize());
            this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), currentTable.getName(), TitledBorder.CENTER, TitledBorder.TOP));
            //System.out.println("changed" + currentTable);
        }
    }
}
