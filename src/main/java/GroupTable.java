import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by RazB on 27/01/2017.
 */
public class GroupTable extends JTable {
    private static final String[] columnNames = {"כיתה",
            "תוצאה",
            "ניקוד"
    };
    private Object[][] data;
    private int tableNum;
    private boolean change;
    private TableModelListener listener;
    private Scores scores;

    public GroupTable(Scores scores, int tableNum) {

        this.tableNum = tableNum;
        this.scores = scores;
        //really needs improvement
        data = new Object[14][3];
        int classNum = 1;
        for (int i = 1; i < 15; i++) {
            data[i - 1][0] = String.format("כיתה %s %d", Application.grade, classNum);
            classNum++;
            data[i - 1][2] = "0";
            data[i - 1][1] = "0";
        }
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        setModel(model);
        setMaximumSize(new Dimension(800, 600));
        setColumnSelectionAllowed(false);
        setRowSelectionAllowed(false);


        listener = e -> {
            if (e.getType() == TableModelEvent.UPDATE || e.getType() == TableModelEvent.INSERT) {
                int selectedColumn = e.getColumn();
                int firstRow = e.getFirstRow();
                if (selectedColumn == 1) {
                    String newPoints;
                    if (Integer.parseInt((String) getValueAt(firstRow, selectedColumn)) == 0) {
                        newPoints = "" + 0;
                    } else {
                        newPoints = "" + ResultArrays.getClosestResult((String) getValueAt(firstRow, selectedColumn), 2, tableNum);
                    }
                    scores.addScore(firstRow, 3, -Integer.parseInt((String) data[firstRow][2]));
                    scores.addScore(firstRow, 3, Integer.parseInt(newPoints));
                    scores.addScore(firstRow, 4, -Integer.parseInt((String) data[firstRow][2]));
                    scores.addScore(firstRow, 4, Integer.parseInt(newPoints));
                    data[firstRow][2] = newPoints;
                    setValueAt(newPoints, firstRow, 2);
                }
                if (data[firstRow][selectedColumn] != null) {
                    if (!data[firstRow][selectedColumn].equals(getValueAt(firstRow, selectedColumn))) {
                        if (selectedColumn == 2) {
                            scores.subtractScore(firstRow, 3, Integer.parseInt((String) data[firstRow][selectedColumn]));
                            scores.addScore(firstRow, 3, Integer.parseInt((String) getValueAt(firstRow, selectedColumn)));
                            scores.subtractScore(firstRow, 4, Integer.parseInt((String) data[firstRow][selectedColumn]));
                            scores.addScore(firstRow, 4, Integer.parseInt((String) getValueAt(firstRow, selectedColumn)));
                        }
                        data[firstRow][selectedColumn] = getValueAt(firstRow, selectedColumn);
                    }
                }
            }
            Application.updateScores();
        };

        addListener();
    }

    ;

    @Override
    public boolean isCellEditable(int row, int column) {
        return !(column == 2);
    }

    public void addListener() {
        getModel().addTableModelListener(listener);
    }

    public void removeListener() {
        getModel().removeTableModelListener(listener);
    }

    public void sortAndPrint() {
        removeListener();
        Object[][] sortedData = data.clone();
        Arrays.sort(sortedData, Comparator.comparing((Object[] entry) -> Integer.parseInt((String) entry[entry.length - 1])).reversed());
        for (int i = 0; i < sortedData.length; i++) {
            for (int j = 0; j < sortedData[i].length; j++) {
                setValueAt(sortedData[i][j], i, j);
            }
        }
        try {
            print();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                setValueAt(data[i][j], i, j);
            }
        }
        addListener();
    }
}
